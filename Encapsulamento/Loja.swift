//
//  Loja.swift
//  Encapsulamento
//
//  Created by Felipe Rocha Oliveira on 10/06/21.
//

import Foundation

enum Pecas {
    case terno, vestido, bone
}

class Loja {
    private var quantidadeDePecas: Int = 0
    private var tipoDePeca: Pecas
    private var preco: Double = 0
    
    init(quantidadeDePecas: Int, tipoDePeca: Pecas) {
        self.quantidadeDePecas = quantidadeDePecas
        self.tipoDePeca = tipoDePeca
        
        self.vender()
    }
    
    func vender() {
        switch self.tipoDePeca {
            case .terno:
                self.preco += Double(self.quantidadeDePecas) * 400
                
                if self.quantidadeDePecas>=3 {
                    self.preco -= 50
                }
            break
            case .vestido:
                self.preco += Double(self.quantidadeDePecas) * 900

                if self.quantidadeDePecas>=5 {
                    print("OBA!!! Ganhou um véu de noiva de brinde")
                }
            break
            case .bone:
                self.preco += Double(self.quantidadeDePecas) * 50
                let bonificaBone = self.quantidadeDePecas / 2
                
                if (bonificaBone>0) {
                    self.preco -= Double(bonificaBone) * 50
                    print("OBA!!! Você ganhou \(bonificaBone) bonés")
                }
            break
        }
    }
    
    public func getPreco() -> Double {
        return self.preco
    }
    
}
