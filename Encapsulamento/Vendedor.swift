//
//  Vendedor.swift
//  Encapsulamento
//
//  Created by Felipe Rocha Oliveira on 10/06/21.
//

import Foundation

class Vendedor {
    private (set) var nome: String?
    private (set) var idade: Int?
    private (set) var cpf: String?
    private (set) var saldoEmConta: Double = 0
    
    init(nome: String, idade: Int, cpf: String, saldoEmConta: Double) {
        self.nome = nome
        self.idade = idade
        self.cpf = cpf
        self.saldoEmConta = saldoEmConta
    }
    
    public func getSaldoVendedor() -> Double {
        return self.saldoEmConta
    }
}
