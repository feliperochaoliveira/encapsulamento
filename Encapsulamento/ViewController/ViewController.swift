//
//  ViewController.swift
//  Encapsulamento
//
//  Created by Felipe Rocha Oliveira on 10/06/21.
//

import UIKit

class ViewController: UIViewController {
    
    var saldo: Double = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.saldo += Loja(quantidadeDePecas: 5, tipoDePeca: .bone).getPreco()
        print("Saldo = \(self.saldo)")

        self.saldo += Loja(quantidadeDePecas: 3, tipoDePeca: .terno).getPreco()
        print("Saldo = \(self.saldo)")

        self.saldo += Loja(quantidadeDePecas: 1, tipoDePeca: .vestido).getPreco()
        print("Saldo = \(self.saldo)")
        
        let funcionario1 = Vendedor(nome: "Felipe", idade: 19, cpf: "08129810928", saldoEmConta: self.saldo)
        
        print("O vendedor \(String(describing: funcionario1.nome)), CPF: \(String(describing: funcionario1.cpf)) ficou com o saldo de vendas no valor de R$ \(funcionario1.getSaldoVendedor())")
    }


}

